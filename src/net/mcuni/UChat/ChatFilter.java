package net.mcuni.UChat;

import net.mcuni.Main;
import net.mcuni.DataManager.Config;

public class ChatFilter
{

	public static Config cf = new Config(Main.instance, "Config");
	
	public static boolean ChatF(String msg) {
		String[] split;
		for (int length = (split = msg.split(" ")).length, i = 0; i < length; ++i) {
            String words = split[i];
            if (cf.getStringList("ChatFilter").contains(words)) 
            	return true;
        }
		return false;
	}
}
