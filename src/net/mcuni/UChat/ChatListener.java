package net.mcuni.UChat;

import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import net.mcuni.Util;
import net.mcuni.API.JsonBuilder;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.HoverEvent;

public class ChatListener implements Listener {
	@EventHandler
	public void onPlayerChat(AsyncPlayerChatEvent e) {
		if (ChatFilter.ChatF(e.getMessage()))
			return;
		e.setCancelled(true);
		Player player = e.getPlayer();

		String chatcolor = Util.replacePAPI(player,ChatConfig.getChatColor());

		List<String> prefixjson = Util.replacePAPI(player,ChatConfig.getPrefixJson());
		List<String> namejson = Util.replacePAPI(player, ChatConfig.getNameJson());
		List<String> suffixjson = Util.replacePAPI(player, ChatConfig.getSuffixJson());


		String prefixcmd = Util.replacePAPI(player, ChatConfig.getPrefixCommand());
		String namecmd = Util.replacePAPI(player, ChatConfig.getNameCommand());
		String suffixcmd = Util.replacePAPI(player, ChatConfig.getSuffixCommand());
		JsonBuilder prefix = new JsonBuilder(Util.replacePAPI(player, ChatConfig.getPrefix()));
		JsonBuilder name = new JsonBuilder(Util.replacePAPI(player, ChatConfig.getPrefix()));
		JsonBuilder suffix = new JsonBuilder(Util.replacePAPI(player, ChatConfig.getPrefix()));

		prefix.setHoverEvent(HoverEvent.Action.SHOW_TEXT, Util.StringListToString(prefixjson))
				.setClickEvent(ClickEvent.Action.RUN_COMMAND, prefixcmd);
		

		name.setHoverEvent(HoverEvent.Action.SHOW_TEXT, Util.StringListToString(namejson))
				.setClickEvent(ClickEvent.Action.RUN_COMMAND, namecmd);
		

		suffix.setHoverEvent(HoverEvent.Action.SHOW_TEXT, Util.StringListToString(suffixjson)).setClickEvent(ClickEvent.Action.RUN_COMMAND, suffixcmd);
		
		prefix.add(name.build()).add(suffix.build()).add(chatcolor+e.getMessage());
		
		
		for(Player target : Bukkit.getOnlinePlayers())
			target.spigot().sendMessage(prefix.build());
	}

}


