package net.mcuni.UChat;

import java.util.List;

import net.mcuni.DataManager.Config;

public class ChatConfig {
	public static Config cf;

	public static String getPrefix() {
		return cf.getString("prefix");
	}

	public static String getName() {
		return cf.getString("name");
	}

	public static String getSuffix() {
		return cf.getString("suffix");
	}

	public static String getChatColor() {
		return cf.getString("chat_color");
	}

	public static List<String> getPrefixJson() {
		return cf.getStringList("prefix_json");
	}

	public static List<String> getNameJson() {

		return cf.getStringList("name_json");
	}

	public static List<String> getSuffixJson() {

		return cf.getStringList("suffix_json");
	}

	public static String getPrefixCommand() {

		return cf.getString("prefix_command");
	}

	public static String getNameCommand() {

		return cf.getString("name_command");
	}

	public static String getSuffixCommand() {

		return cf.getString("suffix_command");
	}

}