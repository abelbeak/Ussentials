package net.mcuni.API;

import net.md_5.bungee.api.chat.*;

public class JsonBuilder {
    private TextComponent tc;
    public JsonBuilder(String str) {
        tc = new TextComponent(str);
    }
    public JsonBuilder setHoverEvent(HoverEvent.Action action, String json) {
        tc.setHoverEvent(new HoverEvent(action, new ComponentBuilder(json).create()));
        return this;
    }
    public JsonBuilder setClickEvent(ClickEvent.Action action, String str) {
        tc.setClickEvent(new ClickEvent(action, str));
        return this;
    }
    public JsonBuilder add(BaseComponent component) {
        tc.addExtra(component);
        return this;
    }
    public JsonBuilder add(String str) {
        tc.addExtra(str);
        return this;
    }
    public TextComponent build() {
        return tc;
    }
}
