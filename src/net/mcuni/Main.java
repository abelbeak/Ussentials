package net.mcuni;

import org.bukkit.Bukkit;
import org.bukkit.event.Listener;
import org.bukkit.plugin.ServicePriority;
import org.bukkit.plugin.java.JavaPlugin;

import net.mcuni.DataManager.Config;
import net.mcuni.UChat.ChatConfig;
import net.mcuni.UChat.ChatListener;
import net.mcuni.USpawn.Setspawn;
import net.mcuni.USpawn.Spawn;
import net.mcuni.USpawn.SpawnConfig;
import net.mcuni.Uconomy.EcoCommand;
import net.mcuni.Uconomy.EcoMain;
import net.mcuni.Ussentials.Command.Gamemode;
import net.mcuni.Ussentials.Command.SetSpeed;
import net.mcuni.Ussentials.Command.Inventory.ClearInventory;
import net.mcuni.Ussentials.Command.TP.TP;
import net.mcuni.Ussentials.Command.TP.Tpa;
import net.mcuni.Ussentials.Command.TP.Tpacancel;
import net.mcuni.Ussentials.Command.TP.Tpaccept;
import net.mcuni.Ussentials.Command.TP.Tpdeny;
import net.mcuni.Ussentials.Command.Warp.DelWarp;
import net.mcuni.Ussentials.Command.Warp.SetWarp;
import net.mcuni.Ussentials.Command.Warp.Warp;
import net.mcuni.Ussentials.Command.Whisper.SocialSpy;
import net.mcuni.Ussentials.Command.Whisper.Whisper;
import net.mcuni.Ussentials.Database.MotdData;
import net.mcuni.Ussentials.Listener.JoinEvent;
import net.mcuni.Ussentials.Listener.QuitEvent;
import net.mcuni.Ussentials.Listener.RespawnEvent;
import net.mcuni.Ussentials.Listener.ServerListListener;
import net.mcuni.Ussentials.Listener.TeleportListener;
import net.milkbowl.vault.economy.Economy;

public class Main extends JavaPlugin implements Listener {

	public static MotdData md;

	public static Main instance;
	public static Economy eco;

	public void onEnable() {
		instance = this;
		createConfig();
		registerEvents();
		commandExecutor();
		pluginCheck();
		Util.ConsoleMessage("Ussentials Plugin has been Enable!");
	}

	public void onDisable() {
		Util.ConsoleMessage("Ussentials Plugin has been Disable");
	}

	private void createConfig() {
		(Util.Message = new Config(this, "Messages")).saveDefault();
		Util.prefix = Util.msg("PREFIX");
		(SpawnConfig.cf = new Config(this, "Spawn")).saveDefault();
		(JoinEvent.cf = new Config(this, "Config")).saveDefault();
		(ChatConfig.cf = new Config(this, "Chat")).saveDefault();
		(ServerListListener.MotdConfig = new Config(this, "Motd")).saveDefault();
		(md = new MotdData(this)).saveDefault();
	}

	private void registerEvents() {
		Bukkit.getPluginManager().registerEvents(new JoinEvent(), this);
		Bukkit.getPluginManager().registerEvents(new QuitEvent(), this);
		Bukkit.getPluginManager().registerEvents(new TeleportListener(), this);
		Bukkit.getPluginManager().registerEvents(new ChatListener(), this);
		Bukkit.getPluginManager().registerEvents(new ServerListListener(), this);
		Bukkit.getPluginManager().registerEvents(new RespawnEvent(), this);
	}

	private void commandExecutor() {
		getCommand("spawn").setExecutor(new Spawn());
		getCommand("setspawn").setExecutor(new Setspawn());
		getCommand("tp").setExecutor(new TP());
		getCommand("tpa").setExecutor(new Tpa());
		getCommand("tpaccept").setExecutor(new Tpaccept());
		getCommand("tpdeny").setExecutor(new Tpdeny());
		getCommand("tpacancel").setExecutor(new Tpacancel());
		getCommand("gamemode").setExecutor(new Gamemode());
		getCommand("gm").setExecutor(new Gamemode());
		getCommand("ci").setExecutor(new ClearInventory());
		getCommand("warp").setExecutor(new Warp());
		getCommand("setwarp").setExecutor(new SetWarp());
		getCommand("delwarp").setExecutor(new DelWarp());
		getCommand("t").setExecutor(new Whisper());
		getCommand("w").setExecutor(new Whisper());
		getCommand("m").setExecutor(new Whisper());
		getCommand("tell").setExecutor(new Whisper());
		getCommand("msg").setExecutor(new Whisper());
		getCommand("socialspy").setExecutor(new SocialSpy());
		getCommand("spy").setExecutor(new SocialSpy());
		getCommand("speed").setExecutor(new SetSpeed());
		getCommand("money").setExecutor(new EcoCommand());
		getCommand("eco").setExecutor(new EcoCommand());
	}

	private void pluginCheck() {
		if (Bukkit.getPluginManager().getPlugin("PlaceHolderAPI") == null)
			Util.ConsoleMessage("Can't find PlaceHolderAPI.");
		if (Bukkit.getPluginManager().getPlugin("Vault") == null)
			Util.ConsoleMessage("Can't find Vault.");
		else
			getServer().getServicesManager().register(Economy.class, new EcoMain(), this, ServicePriority.Normal);

	}
}
