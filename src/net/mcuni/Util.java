package net.mcuni;

import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.clip.placeholderapi.PlaceholderAPI;
import net.mcuni.DataManager.Config;



public class Util {

	static Config Message;

	static String prefix;

	public static void ConsoleMessage(String msg) {
		// Bukkit.getConsoleSender().sendMessage(prefix + msg);
		sendMessage(Bukkit.getConsoleSender(), msg);
	}

	public static void broadcastMessage(String msg) {
		Bukkit.broadcastMessage(msg);
	}

	public static void sendMessage(CommandSender sender, String msg) {
		sender.sendMessage(prefix + msg);
	}

	public static boolean isInteger(String string) {
		return string.matches("[-]?[0-9]+");
	}

	public static boolean isIntegerPositive(String string) {
		return string.matches("[0-9]+");
	}

	public static boolean isDouble(String string) {
		return string.matches("[-]?([0-9]+|[0-9]+[.][0-9]+)");
	}

	public static boolean isDoublePositive(String string) {
		return string.matches("([0-9]+|[0-9]+[.][0-9]+)");
	}

	public static String replaceColor(String s) {
        return s.replace("&1", "§1").replace("&2", "§2").replace("&3", "§3").replace("&4", "§4").replace("&5", "§5")
                .replace("&6", "§6").replace("&7", "§7").replace("&8", "§8").replace("&9", "§9").replace("&0", "§0")
                .replace("&a", "§a").replace("&b", "§b").replace("&c", "§c").replace("&d", "§d").replace("&e", "§e")
                .replace("&f", "§f").replace("&r", "§r").replace("&l", "§l").replace("&o", "§o").replace("&m", "§m").replace("&n","§n");
    }

	public static Location StringToLocation(String s) {
		try {
			String[] cut = s.split(":");
			return new Location(Bukkit.getWorld(cut[0]), Double.parseDouble(cut[1]), Double.parseDouble(cut[2]),
					Double.parseDouble(cut[3]), (float) Double.parseDouble(cut[4]), (float) Double.parseDouble(cut[5]));
		} catch (Exception ex) {
			return null;
		}
	}

	public static String LocationToString(Location loc) {
		return loc.getWorld().getName() + ":" + loc.getX() + ":" + loc.getY() + ":" + loc.getZ() + ":" + loc.getYaw()
				+ ":" + loc.getPitch();
	}

	public static String StringListToString(List<String> list) {
		StringBuffer sb = new StringBuffer();
		for (String s : list)
			sb.append(s).append("\n");
		return sb.toString();
	}

	public static String msg(String key) {
		return replaceColor(Message.getString(key));
	}

	public static String CONSOLEDENY() {
		return "&c이 명령어는 콘솔에서 사용할 수 없습니다.";
	}

	public static String PLAYERISNOTFOUND() {
		return "&c플레이어를 찾을 수 없습니다.";
	}

	public static String PER(String permission) {
		return msg("PERMISSION").replace("%permission%", permission);
	}

	public static String replacePAPI(Player p, String papi) {
		return PlaceholderAPI.setPlaceholders(p, papi);
	}

	public static List<String> replacePAPI(Player p, List<String> papis) {
		return PlaceholderAPI.setPlaceholders(p, papis);
	}
}
