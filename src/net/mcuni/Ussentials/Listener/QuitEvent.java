package net.mcuni.Ussentials.Listener;

import static net.mcuni.Util.msg;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

import net.mcuni.Ussentials.Database.PlayerConfig;

public class QuitEvent implements Listener
{

	public static PlayerConfig pcf;
	
	@EventHandler
	public void onPlayerQuit(PlayerQuitEvent e) 
	{
		Player player = e.getPlayer();
		e.setQuitMessage(msg("QUIT").replace("%player%", player.getName()));
	}
}
