package net.mcuni.Ussentials.Listener;

import static net.mcuni.Util.ConsoleMessage;
import static net.mcuni.Util.msg;
import static net.mcuni.Util.sendMessage;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import net.mcuni.Main;
import net.mcuni.DataManager.Config;
import net.mcuni.USpawn.SpawnConfig;
import net.mcuni.Uconomy.EcoMain;
import net.mcuni.Ussentials.Database.PlayerConfig;

public class JoinEvent implements Listener {

	public static Config cf;
	public static PlayerConfig pcf;

	private static EcoMain eco = new EcoMain();

	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent e) 
	{
		Player player = e.getPlayer();
		
		if (cf.getString("Player-Data.Storage").equalsIgnoreCase("file")) // 
		{
			pcf = new PlayerConfig(player);
			if (pcf.getPlayerName() == null) 
			{
				ConsoleMessage("Create new file " + player.getUniqueId() + ".yml");
				e.setJoinMessage(msg("FIRSTJOIN").replace("%player%", player.getName()));
			}
			else 
				e.setJoinMessage(msg("JOIN").replace("%player%", player.getName()));
			
			
			if (!eco.hasAccount(player.getName()))
				eco.createPlayerAccount(player.getName());
			pcf.setPlayerName();
			pcf.setPlayerIP();
			pcf.setPlayerWebAccount();
			pcf.setPlayerDiscord();
		}
		
		Main.md.setName(pcf.getPlayerIP(), pcf.getPlayerName()); //motd.dat set file

		if (cf.getBoolean("JOINSPAWN")) {
			Location spawn = SpawnConfig.loadSpawn();
			if (spawn == null) {
				sendMessage(player, msg("SPAWNNOTFOUND"));
				return;
			}
			player.teleport(spawn);
			sendMessage(player, msg("SPAWN"));
		}

	}
}
