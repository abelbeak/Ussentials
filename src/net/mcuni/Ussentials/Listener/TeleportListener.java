package net.mcuni.Ussentials.Listener;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerTeleportEvent;

import net.mcuni.API.Particle.ParticleEffect;

public class TeleportListener implements Listener 
{
	@EventHandler
	public void onTeleport(PlayerTeleportEvent e) 
	{
		Player player = e.getPlayer();
		Location loc = player.getLocation().clone();
		ParticleEffect.FLAME.display(0, 0, 0, 0, 1, loc, player);
	}
}