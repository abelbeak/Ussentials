package net.mcuni.Ussentials.Listener;

import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.server.ServerListPingEvent;

import net.mcuni.Main;
import net.mcuni.Util;
import net.mcuni.DataManager.Config;

public class ServerListListener implements Listener {
	public static Config MotdConfig;

	@EventHandler(priority = EventPriority.MONITOR)

	public void onServerPing(ServerListPingEvent e) {
		String motd1 = Util.replaceColor(
				MotdConfig.getString("LINE1").replace("%player%", Main.md.getName(e.getAddress().getHostAddress())));
		String motd2 = Util.replaceColor(
				MotdConfig.getString("LINE2").replace("%player%", Main.md.getName(e.getAddress().getHostAddress())));

		e.setMotd(motd1 + "\n" + motd2);
	}
}
