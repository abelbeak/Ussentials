package net.mcuni.Ussentials.Listener;

import static net.mcuni.Util.msg;
import static net.mcuni.Util.sendMessage;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerRespawnEvent;

import net.mcuni.USpawn.SpawnConfig;

public class RespawnEvent implements Listener
{
	@EventHandler
	public void PlayerRespawnEvent(PlayerRespawnEvent e) 
	{
		Player player = e.getPlayer();
		Location spawn = SpawnConfig.loadSpawn();
		if (spawn == null) 
		{
			sendMessage(player, msg("SPAWNNOTFOUND"));
			return;
		}
		player.teleport(spawn);
        sendMessage(player, msg("SPAWN"));
	}

}
