package net.mcuni.Ussentials.Command;

import static net.mcuni.Util.*;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class SetSpeed implements CommandExecutor {

	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (label.equalsIgnoreCase("speed")) {
			if (!sender.hasPermission("Uni.speed")) {
				sendMessage(sender, PER("Uni.speed"));
				return false;
			}
			if (args.length == 0 || args.length == 1 && !isInteger(args[0])) {
				sendMessage(sender, msg("SPEEDHELP"));
				return false;
			}
			Player target;
			if (args.length != 3) {
				if (!(sender instanceof Player)) {
					sendMessage(sender, CONSOLEDENY());
					sendMessage(sender, msg("SPEEDHELP"));
					return false;
				}
				target = (Player) sender;
			} else
				target = Bukkit.getPlayer(args[2]);
			if (target == null) {
				sendMessage(sender, PLAYERISNOTFOUND());
				return false;
			}
			if (args.length == 1) {
				if (!checkSpeed(sender, args[0]))
					return false;
				int speed = Integer.parseInt(args[0]);
				if (target.isFlying()) {
					setFlySpeed(target, speed);
					return false;
				}
				setWalkSpeed(target, speed);
				return false;
			}
			if (!checkSpeed(sender, args[1]))
				return false;
			int speed = Integer.parseInt(args[1]);
			if (args[0].equalsIgnoreCase("walk")) {
				setWalkSpeed(target, speed);
				return false;
			}
			if (args[0].equalsIgnoreCase("fly")) {
				setFlySpeed(target, speed);
				return false;
			}

			sendMessage(sender, msg("SPEEDHELP"));
		}
		return false;
	}

	private void setWalkSpeed(Player player, int speed) {
		player.setWalkSpeed(Float.parseFloat(speed == 1 ? "0.2" : String.valueOf(0.2 + 0.08 * speed)));
		sendMessage(player, msg("WALKSPEED").replace("%player%", player.getName()).replace("%speed%", speed + ""));
	}

	private void setFlySpeed(Player player, int speed) {
		player.setFlySpeed(Float.parseFloat(String.valueOf(0.1 * speed)));
		sendMessage(player, msg("FLYSPEED").replace("%player%", player.getName()).replace("%speed%", speed + ""));
	}

	private boolean checkSpeed(CommandSender sender, String s) {
		if (!isInteger(s)) {
			sendMessage(sender, msg("ISNOTINTEGER"));
			return false;
		}
		int speed = Integer.parseInt(s);
		if (speed <= 0 || speed > 10) {
			sendMessage(sender, msg("SPEEDLIMIT"));
			return false;
		}
		return true;
	}

}
