package net.mcuni.Ussentials.Command;

import static net.mcuni.Util.*;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Gamemode implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

		if (label.equalsIgnoreCase("gm") || label.equalsIgnoreCase("gamemode")) {
			if (!sender.hasPermission("Gamemode.use")) {
				sendMessage(sender, PER("Gamemode.use"));
				return false;
			}
			if (args.length == 0) {
				sendMessage(sender, msg("GAMEMODEHELP"));
				return false;
			}
			Player target;

			if (args.length != 2) {
				if (!(sender instanceof Player)) 
				{
					sendMessage(sender, CONSOLEDENY());
					return false;
				}
				target = (Player) sender;
			}
			else
				target = Bukkit.getPlayer(args[1]);


			if (target != null) 
			{
				GameMode gm = null;

				if (args[0].equals("0") || args[0].equalsIgnoreCase("survival")) {
					gm = GameMode.SURVIVAL;
				} else if (args[0].equals("1") || args[0].equalsIgnoreCase("creative")) {
					gm = GameMode.CREATIVE;
				} else if (args[0].equals("2") || args[0].equalsIgnoreCase("adventure")) {
					gm = GameMode.ADVENTURE;
				} else if (args[0].equals("3") || args[0].equalsIgnoreCase("spectator")) {
					gm = GameMode.SPECTATOR;
				}

				if (gm == null) {
					sendMessage(sender, msg("GAMEMODEHELP"));
					return false;
				}

				target.setGameMode(gm);
				sendMessage(sender, msg("GAMEMODE").replace("%player%", target.getName()).replace("%mode%", gm.name())); 
				if (sender != target)
					sendMessage(target, msg("GAMEMODE").replace("%player%", target.getName()).replace("%mode%", gm.name())); 

				return false;
			} else {
				sendMessage(sender, PLAYERISNOTFOUND());
				return false;
			}
		}
		return false;
	}
}
