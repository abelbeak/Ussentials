package net.mcuni.Ussentials.Command.Inventory;

import static net.mcuni.Util.CONSOLEDENY;
import static net.mcuni.Util.PER;
import static net.mcuni.Util.PLAYERISNOTFOUND;
import static net.mcuni.Util.msg;
import static net.mcuni.Util.sendMessage;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class ClearInventory implements CommandExecutor {

	@Override

	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

		if (label.equalsIgnoreCase("ci")) 
		{
			if (!sender.hasPermission("Uni.ci")) {
				sendMessage(sender, PER("Uni.ci"));
				return false;
			}
			Player target;
			if (args.length != 1) {
				if (!(sender instanceof Player)) { 
					sendMessage(sender, CONSOLEDENY());
					return false;
				}
				target = (Player) sender;
			} 
			else
				target = Bukkit.getPlayer(args[0]);

			if (target != null) 
			{
				target.getInventory().clear();
				clearArmor(target);
				sendMessage(sender, target+ "님의 인벤토리와 갑옷을 제거했습니다");
				if (sender != target)
					sendMessage(target, msg("CLEARINVENTORYTARGET"));
				return false;
			}
			sendMessage(sender, PLAYERISNOTFOUND());
			return false;
		}
		return false;
	}

	private void clearArmor(Player player) {
		player.getInventory().setHelmet(null);
		player.getInventory().setChestplate(null);
		player.getInventory().setLeggings(null);
		player.getInventory().setBoots(null);
	}
}
