package net.mcuni.Ussentials.Command.TP;

import static net.mcuni.Util.*;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.mcuni.Main;

public class Tpa implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (label.equalsIgnoreCase("tpa")) {
			if (!(sender instanceof Player)) {
				sendMessage(sender, CONSOLEDENY());
				return false;
			}

			if (!sender.hasPermission("Uni.tpa")) {
				sendMessage(sender, PER("Uni.tpa"));
				return false;
			}
			if (args.length != 1) {
				sendMessage(sender, msg("TPAHELP"));
				return false;
			}
			Player player = (Player) sender;
			Player target = Bukkit.getPlayer(args[0]);

			if (target == null) {
				sendMessage(sender, PLAYERISNOTFOUND());
				return false;
			}

			if (TeleportInvite.sendTPA(player) != null) {
				sendMessage(player, msg("TPASENDERALREADYSEND").replace("%player%", target.getName()));
				return false;
			}

			if (TeleportInvite.recieveTPA(target) != null) {
				sendMessage(player, msg("TPATARGETALREADYRECEIVE").replace("%player%", target.getName()));
				return false;
			}

			sendMessage(player, msg("TPASEND").replace("%player%", target.getName()));
			sendMessage(target, msg("TPARECEIVE").replace("%player%", target.getName()));

			TeleportInvite invite = new TeleportInvite(player, target);

			Bukkit.getScheduler().scheduleSyncDelayedTask(Main.instance, new Runnable() { // 스케쥴러

				@Override
				public void run() {
					if (TeleportInvite.sendTPA(player) != null) {
						sendMessage(player, msg("TPASENDTIMEOUT"));
						sendMessage(target, msg("TPARECIEVETIMEOUT").replace("%player%", player.getName()));
						invite.removeInvite();
					}
				}
			}, 10 * 20);

			return false;
		}
		return false;
	}
}
