package net.mcuni.Ussentials.Command.TP;

import static net.mcuni.Util.*;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Tpaccept implements CommandExecutor {
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

		if (label.equalsIgnoreCase("tpaccept")) {
			if (!(sender instanceof Player)) {
				sendMessage(sender, CONSOLEDENY());
				return false;
			}

			if (!sender.hasPermission("Uni.tpaccpet")) {
				sendMessage(sender, PER("Uni.tpaccept"));
				return false;
			}

			Player player = (Player) sender;
			TeleportInvite invite = TeleportInvite.recieveTPA(player);

			if (invite == null) {
				sendMessage(sender, msg("TPACCEPTNOTHAVE"));
				return false;
			}

			invite.getSender().teleport(invite.getTarget());
			sendMessage(invite.getTarget(), msg("TPACCEPT").replace("%player%", invite.getSender().getName()));
			sendMessage(invite.getTarget(), msg("TPMESSAGE").replace("%player%", invite.getTarget().getName()));
			invite.removeInvite();
			return false;
		}

		return false;
	}

}
