package net.mcuni.Ussentials.Command.TP;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.entity.Player;

public class TeleportInvite {

	private Player sender;
	private Player target;

	public static List<TeleportInvite> invites = new ArrayList<>();// << 이거쓰는거

	public TeleportInvite(Player sender, Player target) // 생성자
	{
		this.sender = sender;
		this.target = target;
		invites.add(this); // this는 현재 객체임
	}

	public Player getSender() {
		return sender;
	}

	public Player getTarget() {
		return target;
	}

	public void removeInvite() {
		invites.remove(this);
	}

	// 보낸 초대를 받아옴
	public static TeleportInvite sendTPA(Player sender) {
		for (TeleportInvite invite : invites)
			if (invite.getSender() == sender)
				return invite;
		return null;
	}

	public static TeleportInvite recieveTPA(Player target) {
		for (TeleportInvite invite : invites)
			if (invite.getTarget() == target)
				return invite;
		return null;

	}

}
