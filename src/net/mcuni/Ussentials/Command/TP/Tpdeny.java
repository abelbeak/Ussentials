package net.mcuni.Ussentials.Command.TP;

import static net.mcuni.Util.*;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Tpdeny implements CommandExecutor {
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (label.equalsIgnoreCase("tpdeny")) {

			if (!(sender instanceof Player)) {
				sendMessage(sender, CONSOLEDENY());
				return false;
			}

			if (!sender.hasPermission("Uni.tpdeny")) {
				sendMessage(sender, PER("Uni.tpdeny"));
				return false;
			}

			Player player = (Player) sender;

			TeleportInvite invite = TeleportInvite.recieveTPA(player);

			if (invite == null) {
				sendMessage(sender, msg("TPACCEPTNOTHAVE"));
				return false;
			}

			sendMessage(invite.getTarget(), msg("TPDENY").replace("%player%", invite.getSender().getName()));
			sendMessage(invite.getSender(), msg("TPDENYSENDER"));
			invite.removeInvite();
			return false;
		}

		return false;
	}

}
