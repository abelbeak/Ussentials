package net.mcuni.Ussentials.Command.TP;

import static net.mcuni.Util.*;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class TP implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

		if (label.equalsIgnoreCase("tp")) {

			if (!(sender instanceof Player)) {
				sendMessage(sender, CONSOLEDENY());
				return false;
			}

			Player player = (Player) sender;

			if (!player.hasPermission("Uni.tp")) {
				sendMessage(player, PER("Uni.tp"));
				return false;
			}

			if (args.length != 1) {
				sendMessage(player, msg("TPHELP"));
				return false;
			}

			Player target = Bukkit.getPlayer(args[0]);

			if (target == null) {
				sendMessage(player, PLAYERISNOTFOUND());
				return false;
			}

			player.teleport(target);
			sendMessage(player, msg("TPMESSAGE").replace("%player%", target.getName()));
			return false;
		}
		return false;
	}

}
