package net.mcuni.Ussentials.Command.TP;

import static net.mcuni.Util.*;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Tpacancel implements CommandExecutor {
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (label.equalsIgnoreCase("tpacancel")) {
			if (!(sender instanceof Player)) {
				sendMessage(sender, CONSOLEDENY());
				return false;
			}

			if (!sender.hasPermission("Uni.tpacancel")) {
				sendMessage(sender, PER("Uni.tpacancel"));
				return false;
			}

			Player player = (Player) sender;

			TeleportInvite invite = TeleportInvite.sendTPA(player);

			if (invite == null) {
				sendMessage(sender, msg("TPANOTSEND"));
				return false;
			}

			sendMessage(invite.getTarget(), msg("TPACANCELTARGET").replace("%player%", invite.getSender().getName()));
			sendMessage(invite.getTarget(), msg("TPACANCELSENDER").replace("%player%", invite.getSender().getName()));

			invite.removeInvite();
			return false;
		}
		return false;
	}
}
