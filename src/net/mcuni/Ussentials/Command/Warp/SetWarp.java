package net.mcuni.Ussentials.Command.Warp;

import static net.mcuni.Util.CONSOLEDENY;
import static net.mcuni.Util.PER;
import static net.mcuni.Util.msg;
import static net.mcuni.Util.sendMessage;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.mcuni.Ussentials.Database.WarpConfig;

public class SetWarp implements CommandExecutor {
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

		if (label.equalsIgnoreCase("setwarp")) {
			if (!(sender instanceof Player)) {
				sendMessage(sender, CONSOLEDENY());
				return false;
			}
			if (!sender.hasPermission("uni.setwarp")) {
				sendMessage(sender, PER("uni.setwarp"));
				return false;
			}

			Player player = (Player) sender;

			if (args.length != 1) {
				sendMessage(player, msg("SETWARPHELP"));
				return false;
			}

			WarpConfig warp = new WarpConfig(args[0]);
			warp.setLocation(player.getLocation());
			sendMessage(player, msg("SETWARP").replace("%warp%", args[0]));
			return false;
		}
		return false;
	}

}
