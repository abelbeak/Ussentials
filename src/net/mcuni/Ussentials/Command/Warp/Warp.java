package net.mcuni.Ussentials.Command.Warp;

import static net.mcuni.Util.CONSOLEDENY;
import static net.mcuni.Util.PER;
import static net.mcuni.Util.PLAYERISNOTFOUND;
import static net.mcuni.Util.msg;
import static net.mcuni.Util.sendMessage;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.mcuni.Ussentials.Database.WarpConfig;

/*
 * 기본적으로 /warp %warp% %player%
 * 이고 만약 %player% 을 안적었다면 플레이어는 %warp% 로 이동되게 한다.
 * 만약 콘솔이 %player%을 안적었다면 
 *command args[0] args[1]
 */

public class Warp implements CommandExecutor {
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

		if (label.equalsIgnoreCase("warp")) {

			if (!sender.hasPermission("Uni.warp")) {
				sendMessage(sender, PER("Uni.warp"));
				return false;
			}

			if (args.length == 0) {
				sendMessage(sender, msg("WARPHELP"));
				return false;
			}

			Player target = null;
			if (args.length != 2) {
				if (!(sender instanceof Player)) {
					sendMessage(sender, CONSOLEDENY());
					return false;
				}
				target = (Player) sender;
			} else
				target = Bukkit.getPlayer(args[1]);

			if (!WarpConfig.containsWarp(args[0])) {
				sendMessage(sender, msg("WARPNOTFOUND").replace("%warp%", args[0]));
				return false;
			} // 워프가 있는지 확인하고 없으면 취소시킵니다.

			WarpConfig warp = new WarpConfig(args[0]); // warp 변수에

			if (target != null) {

				target.teleport(warp.getLocation());

				sendMessage(target, msg("WARP").replace("%warp%", args[0]));

				if (target != sender) {
					sendMessage(sender,
							msg("WARPTARGET").replace("%warp%", args[0]).replace("%player%", target.getName()));
					return false;
				}
			} else {
				sendMessage(sender, PLAYERISNOTFOUND());
				return false;
			}

			return false;
		}
		return false;
	}

}
