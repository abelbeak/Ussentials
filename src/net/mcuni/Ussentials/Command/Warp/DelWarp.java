package net.mcuni.Ussentials.Command.Warp;

import static net.mcuni.Util.CONSOLEDENY;
import static net.mcuni.Util.PER;
import static net.mcuni.Util.msg;
import static net.mcuni.Util.sendMessage;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.mcuni.Ussentials.Database.WarpConfig;

public class DelWarp implements CommandExecutor {
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

		if (label.equalsIgnoreCase("delwarp")) {

			if (!(sender instanceof Player)) {
				sendMessage(sender, CONSOLEDENY());
				return false;
			}
			if (!sender.hasPermission("uni.delwarp")) {
				sendMessage(sender, PER("uni.delwarp"));
				return false;
			}

			if (args.length != 1) {
				sendMessage(sender, msg("DELWARPHELP"));
				return false;
			}

			if (WarpConfig.containsWarp(args[0])) {

				WarpConfig warp = new WarpConfig(args[0]);

				warp.remove();
				sendMessage(sender, msg("DELWARP").replace("%warp%", args[0]));
				return false;
			}

			sendMessage(sender, msg("WARPNOTFOUND").replace("%warp%", args[0]));
			return false;

		}
		return false;
	}

}
