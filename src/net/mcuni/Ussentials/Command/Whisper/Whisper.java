package net.mcuni.Ussentials.Command.Whisper;

import static net.mcuni.Util.*;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Whisper implements CommandExecutor {
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

		if (label.equalsIgnoreCase("w") || label.equalsIgnoreCase("msg") || label.equalsIgnoreCase("m")
				|| label.equalsIgnoreCase("tell") || label.equalsIgnoreCase("t")) {
			if (args.length <= 1) {
				sendMessage(sender, msg("WHISPERHELP"));
				return false;
			}

			Player target = Bukkit.getPlayer(args[0]);

			if (target == null) {
				sendMessage(sender, PLAYERISNOTFOUND());
				return false;
			}

			StringBuilder msg = new StringBuilder();
			for (int i = 1; i < args.length; i++)
				msg.append(args[i] + " ");

			sendMessage(sender,
					msg("WHISPERSENDER").replace("%player%", target.getName()).replace("%message%", msg.toString()));
			sendMessage(target,
					msg("WHISPERTARGET").replace("%player%", sender.getName()).replace("%message%", msg.toString()));
			for (Player spy : SocialSpy.spyList)
				sendMessage(spy, msg("WHISPERSPY").replace("%sender%", sender.getName())
						.replace("%target%", target.getName()).replace("%message%", msg.toString()));
			return false;
		}
		return false;
	}

}
