package net.mcuni.Ussentials.Command.Whisper;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import static net.mcuni.Util.*;

import java.util.ArrayList;
import java.util.List;

public class SocialSpy implements CommandExecutor {
	static List<Player> spyList = new ArrayList<>();

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (label.equalsIgnoreCase("spy") || label.equalsIgnoreCase("socialspy")) {
			if (!sender.hasPermission("uni.socialspy")) {
				sendMessage(sender, PER("uni.socialspy"));
				return false;
			}
			if (!(sender instanceof Player)) {
				sendMessage(sender, CONSOLEDENY());
				return false;
			}
			if (spyList.contains(sender)) {
				sendMessage(sender, msg("SPYMODEOFF"));
				spyList.remove(sender);
			} else {
				sendMessage(sender, msg("SPYMODEON"));
				spyList.add((Player) sender);
			}
			return false;
		}
		return false;

	}
}
