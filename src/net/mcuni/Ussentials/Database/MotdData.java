package net.mcuni.Ussentials.Database;

import org.bukkit.plugin.java.*;
import org.bukkit.configuration.file.*;
import java.io.*;

public class MotdData {

	private String path;
	private JavaPlugin plugin;
	private YamlConfiguration data;
	private File file;

	public MotdData(JavaPlugin plugin) {
		this.path = "plugins\\" + plugin.getDescription().getName() + "\\Motd.dat";
		this.plugin = plugin;
		reload();
	}

	public void save() {
		if (data != null && file != null) {
			try {
				data.save(file);
			} catch (IOException e) {

			}
		}
	}

	public void reload() {
		if (file == null)
			file = new File(path);
		data = YamlConfiguration.loadConfiguration(file);
		InputStream is = plugin.getResource("Motd.dat");
		if (is != null)
			data.setDefaults(
					YamlConfiguration.loadConfiguration(new InputStreamReader(plugin.getResource("Motd.dat"))));
	}

	public void saveDefault() {
		if (!new File(path).exists())
			plugin.saveResource("Motd.dat", false);
		reload();
	}

	public void setName(String ip, Object value) {
		data.set(ip.replace(".", "-"), value);
		save();
	}

	public String getName(String ip) {
		return data.getString(ip.replace(".", "-")) == null ? "" : data.getString(ip.replace(".", "-"));
	}

}
