package net.mcuni.Ussentials.Database;

import java.util.LinkedHashMap;
import java.util.Map;

import org.bukkit.entity.Player;

import net.mcuni.Main;
import net.mcuni.DataManager.Config;

public class PlayerConfig {

	private static Map<String, Config> configTemp = new LinkedHashMap<>();

	private Player player;
	private static Config cf;

	public PlayerConfig(Player player) {
		this(player.getUniqueId().toString());
		this.player = player;
	}

	public PlayerConfig(String uuid) {
		uuid = uuid.replace("-", "");
		PlayerConfig.cf = configTemp.get(uuid);
		if (!configTemp.containsKey(uuid))
			configTemp.put(uuid, PlayerConfig.cf = new Config(Main.instance, "playerdata\\" + uuid));
	}

	public void setPlayerName() {
		cf.set("PlayerName", player.getName());
		cf.save();
	}

	public void setPlayerIP() {
		cf.set("IPAdress", player.getAddress().getAddress().getHostAddress());
		cf.save();
	}
	public boolean isWebAccount() 
	{	
		if(cf.isSet("WebAccount")) 
		{
			return true;
		}
		return false;
	}
	public void setPlayerWebAccount() {
		cf.set("WebAccount", "");
		cf.save();
	}
	public void setPlayerDiscord() {
		cf.set("Discord", "");
		cf.save();
	}
	public String getPlayerName() {
		return cf.getString("PlayerName");
	}

	public String getPlayerIP() {
		return cf.getString("IPAdress");
	}
	
	public static String getPlayerWebAccount() {
		return cf.getString("WebAccount");
	}
	
	public String getPlayerDiscord() {
		return cf.getString("Discord");
	}

	public boolean isFileExists() {
		return cf.isExists();
	}

	public boolean contains(String path) {
		return cf.contains(path);
	}

	public String getString(String path) {
		return cf.getString(path);
	}
	
	public double getDouble(String path) {
		return cf.getDouble(path);
	}

	public void set(String path, Object value) {
		cf.set(path, value);
		cf.save();
	}

}
