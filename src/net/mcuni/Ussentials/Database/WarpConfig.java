package net.mcuni.Ussentials.Database;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Location;

import net.mcuni.Main;
import net.mcuni.DataManager.Config;

public class WarpConfig {

	private String name;
	public Config cf;

	public WarpConfig(String name) {
		this.name = name; // 이 객체의 name 변수를 인자로 받은 name으로 설정
		this.cf = new Config(Main.instance, "warps\\" + name); // warps\워프명.yml 파일을 조작하는 콘피그 변수인 cf 설정
	}

	public void remove() {
		cf.delete(); // 콘피그 파일 지움
	}

///////////////////////////////////////////////////////////////////////////////////////////////////////////

	public void setWorld(String world) {
		cf.set("world", world); // 콘피그에 world를 설정
		cf.save();
	}

	public void setX(double d) {
		cf.set("x", d); // x 설정
		cf.save();
	}

	public void setY(double d) {
		cf.set("y", d); // y 설정
		cf.save();
	}

	public void setZ(double d) {
		cf.set("z", d); // z 설정
		cf.save();
	}

	public void setYaw(float f) {
		cf.set("yaw", f); // float 설정
		cf.save();
	}

	public void setPitch(float f) {
		cf.set("pitch", f); // pitch 설정
		cf.save();
	}

	public void setLocation(Location loc) { // loc 변수를 이용하여 이 워프의 x,y,z,yaw,pitch를 설정함
		setWorld(loc.getWorld().getName());
		setX(loc.getX());
		setY(loc.getY());
		setZ(loc.getZ());
		setYaw(loc.getYaw());
		setPitch(loc.getPitch());
	}

///////////////////////////////////////////////////////////////////////////////////////////////////////////

	public String getWorld() {
		return cf.getString("world"); // world 받아옴
	}

	public double getX() {
		return cf.getDouble("x"); // x 받아옴
	}

	public double getY() {
		return cf.getDouble("y"); // y 받아옴
	}

	public double getZ() {
		return cf.getDouble("z"); // z 받아옴
	}

	public float getYaw() {
		return (float) cf.getDouble("yaw"); // yaw 받아옴
	}

	public float getPitch() {
		return (float) cf.getDouble("pitch"); // pitch 받아옴
	}

	public Location getLocation() {
		return new Location(Bukkit.getWorld(getWorld()), getX(), getY(), getZ(), getYaw(), getPitch()); // 좌표값들을 받아와서
																										// Location으로 만듬
	}

	public String getName() {
		return this.name; // name 리턴
	}

///////////////////////////////////////////////////////////////////////////////////////////////////////////

	public static List<String> getWarps() {
		List<String> l = new ArrayList<>(); // 리스트 만듬
		File dir = new File("plugins\\UniEssentials\\warps\\"); // 디렉토리 받아옴
		File[] files = dir.listFiles(); // 폴더안의 파일들을 받아옴
		if (files == null) // 파일 배열이 null 이라면 (길이가 0인것과 다름!)
			return l; // 리스트 리턴
		for (File f : files) // 파일들을 File f에 넣으며 루프
			l.add(f.getName().replace(".yml", "")); // 파일의 이름에서 .yml을 지운다음 리스트에 넣음
		return l; // 리스트 리턴
	}

	public static boolean containsWarp(String warp) { // 이거
		return getWarps().contains(warp);
	}

}
