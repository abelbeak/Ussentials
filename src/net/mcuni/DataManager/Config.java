package net.mcuni.DataManager;

import org.bukkit.plugin.java.*;
import org.bukkit.configuration.file.*;
import java.io.*;
import org.bukkit.inventory.*;
import org.bukkit.configuration.*;
import java.util.*;

public class Config {
	private String path;
	private JavaPlugin plugin;
	private String filename;
	private YamlConfiguration config;
	private File configfile;

	public Config(JavaPlugin plugin, String filename) {
		this(plugin, plugin.getName(), filename);
	}

	public Config(JavaPlugin plugin, String folder, String filename) {
		this.path = "plugins\\" + folder + "\\";
		this.plugin = plugin;
		this.filename = filename;
		reload();
	}

	public void save() {
		if (config != null) {
			try {
				config.save(configfile);
			} catch (IOException ignored) { }
		}
	}

	public void reload() {
		if (configfile == null)
			configfile = new File(path + filename + ".yml");
		config = YamlConfiguration.loadConfiguration(configfile);
		if (plugin.getResource(filename + ".yml") != null) {
			config.setDefaults(YamlConfiguration.loadConfiguration(configfile));
		}
	}

	public void saveDefault() {
		if (!new File(path + filename + ".yml").exists())
			plugin.saveResource(filename + ".yml", false);
		reload();
	}

	public void delete() {
		configfile.delete();
	}

	public boolean isExists() {
		return configfile.exists();
	}

	public int getInt(String path) {
		return config.getInt(path);
	}

	public int getInt(String path, int def) {
		return config.getInt(path, def);
	}

	public String getString(String path) {
		return config.getString(path);
	}

	public String getString(String path, String def) {
		return config.getString(path, def);
	}

	public ItemStack getItemStack(String path) {
		return config.getItemStack(path);
	}

	public ItemStack getItemStack(String path, ItemStack def) {
		return config.getItemStack(path, def);
	}

	public List<String> getStringList(String path) {
		return config.getStringList(path);
	}

	public List<String> getStringList(String path, List<String> def) {
		return config.contains(path) ? config.getStringList(path) : def;
	}

	public double getDouble(String path) {
		return config.getDouble(path);
	}

	public double getDouble(String path, double def) {
		return config.getDouble(path, def);
	}

	public long getLong(String path) {
		return config.getLong(path);
	}

	public long getLong(String path, long def) {
		return config.getLong(path, def);
	}

	public boolean getBoolean(String path) {
		return config.getBoolean(path);
	}

	public boolean getBoolean(String path, boolean def) {
		return config.getBoolean(path, def);
	}

	public void createSection(String path) {
		config.createSection(path);
	}

	public ConfigurationSection getConfigurationSection(String path) {
		return config.getConfigurationSection(path);
	}

	public List<?> getList(String path) {
		return config.getList(path);
	}

	public boolean contains(String path) {
		return config.contains(path);
	}

	public void removeKey(String path) {
		config.set(path, null);
		save();
	}

	public void set(String path, Object value) {
		config.set(path, value);
	}

	public Set<String> getKeys(boolean deep) {
		return config.getKeys(deep);
	}

	public boolean isSet(String path) {
		return config.isSet(path);
	}

	public boolean isList(String path) {
		return config.isList(path);
	}
}
