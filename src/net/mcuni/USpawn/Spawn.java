package net.mcuni.USpawn;

import static net.mcuni.Util.*;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;

import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Spawn implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (label.equalsIgnoreCase("spawn")) {
			if (!sender.hasPermission("Uni.spawn")) {
				sendMessage(sender, PER("Uni.spawn"));
				return false;
			}

			Player target;

			if (args.length != 2) {
				if (!(sender instanceof Player)) {
					sendMessage(sender, CONSOLEDENY());
					sendMessage(sender, "&e/spawn %player%");
					return false;
				}
				target = (Player) sender;
			} else
				target = Bukkit.getPlayer(args[0]);

			if (SpawnConfig.loadSpawn() == null) {
				sendMessage(sender, "스폰이 설정되어 있지 않습니다.");
				return false;
			}

			if (target != null)
			{
				target.teleport(SpawnConfig.loadSpawn());

				sendMessage(sender, msg("SPAWN"));
				if (sender != target)
					sendMessage(sender, msg("SPAWNTARGET").replace("%player%", target.getName()));

				return false;

			}
			// 플레이어가 오프라인일때
			sendMessage(sender, PLAYERISNOTFOUND());
			return false;
		}

		return false;
	}

}
