package net.mcuni.USpawn;

import static net.mcuni.Util.*;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Setspawn implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (label.equalsIgnoreCase("setspawn")) {
			if (!(sender instanceof Player)) {
				sendMessage(sender, CONSOLEDENY());
				return false;
			}

			if (!sender.hasPermission("Uni.setspawn")) {
				sendMessage(sender, PER("Uni.setspawn"));
				return false;
			}

			Player player = (Player) sender;

			SpawnConfig.saveSpawn(player.getLocation());
			sendMessage(player, "&f현재 위치를 스폰장소로 지정하였습니다.");
			return false;
		}
		return false;
	}

}
