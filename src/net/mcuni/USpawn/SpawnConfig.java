package net.mcuni.USpawn;

import org.bukkit.Location;

import net.mcuni.Util;
import net.mcuni.DataManager.Config;

public class SpawnConfig {
	public static Config cf;

	public static Location loadSpawn() // 문자를 스폰으로
	{
		return !cf.contains("Spawn") ? null : Util.StringToLocation(cf.getString("Spawn"));
	}

	public static void saveSpawn(Location loc) // 위치를 문자로
	{
		cf.set("Spawn", Util.LocationToString(loc));
		cf.save();
	}
}
