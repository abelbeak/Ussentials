package net.mcuni.Uconomy;

import java.text.DecimalFormat;
import java.util.List;

import org.bukkit.Bukkit;

import net.mcuni.Main;
import net.mcuni.Ussentials.Database.PlayerConfig;
import net.milkbowl.vault.economy.AbstractEconomy;
import net.milkbowl.vault.economy.EconomyResponse;

@SuppressWarnings("deprecation")
public class EcoMain extends AbstractEconomy {

	private static PlayerConfig pcf;
	private static DecimalFormat df = new DecimalFormat("#,###");

	public boolean hasAccount(String name) {
		pcf = new PlayerConfig(Bukkit.getOfflinePlayer(name).getUniqueId().toString());
		return pcf.contains("Money");
	}

	public double getBalance(String name) {
		pcf = new PlayerConfig(Bukkit.getOfflinePlayer(name).getUniqueId().toString());
		return pcf.getDouble("Money");
	}

	// 보유 여부
	public boolean has(String name, double amount) {
		return getBalance(name) >= amount;
	}

	public EconomyResponse set(String name, double amount) {
		pcf = new PlayerConfig(Bukkit.getPlayer(name));
		pcf.set("Money", amount);
		return new EconomyResponse(amount, 0.0, EconomyResponse.ResponseType.SUCCESS, "");
	}

	// 뺏기
	public EconomyResponse withdrawPlayer(String name, double amount) {
		if (!hasAccount(name))
			return new EconomyResponse(0.0, 0.0, EconomyResponse.ResponseType.FAILURE,
					"The player does not have an account!");
		double balance = getBalance(name);
		if (!has(name, amount))
			return new EconomyResponse(0.0, balance, EconomyResponse.ResponseType.FAILURE,
					"The value is more than the player's balance!");
		return set(name, balance - amount);
	}

	// 주기
	public EconomyResponse depositPlayer(String name, double amount) {
		if (!hasAccount(name))
			return new EconomyResponse(0.0, 0.0, EconomyResponse.ResponseType.FAILURE,
					"The player does not have an account!");
		double balance = getBalance(name);
		if (amount < 0)
			return new EconomyResponse(0.0, balance, EconomyResponse.ResponseType.FAILURE, "Value is less than zero");
		return set(name, balance + amount);
	}

	public boolean createPlayerAccount(String name) {
		set(name, 0);
		return true;
	}

	public String format(double summ) {
		return df.format(summ);
	}

	public boolean hasAccount(String name, String world) {
		return this.hasAccount(name);
	}

	public boolean has(String name, String world, double amount) {
		return this.has(name, amount);
	}

	public double getBalance(String name, String world) {
		return this.getBalance(name);
	}

	public EconomyResponse withdrawPlayer(String name, String world, double amount) {
		return this.withdrawPlayer(name, amount);
	}

	public EconomyResponse depositPlayer(String name, String world, double amount) {
		return this.depositPlayer(name, amount);
	}

	public EconomyResponse createBank(String s, String s1) {
		return null;
	}

	public EconomyResponse deleteBank(String s) {
		return null;
	}

	public EconomyResponse bankBalance(String s) {
		return null;
	}

	public EconomyResponse bankHas(String s, double v) {
		return null;
	}

	public EconomyResponse bankWithdraw(String s, double v) {
		return null;
	}

	public EconomyResponse bankDeposit(String s, double v) {
		return null;
	}

	public EconomyResponse isBankOwner(String s, String s1) {
		return null;
	}

	public EconomyResponse isBankMember(String s, String s1) {
		return null;
	}

	public boolean createPlayerAccount(String name, String world) {
		return this.createPlayerAccount(name);
	}

	public List<String> getBanks() {
		return null;
	}

	public boolean hasBankSupport() {
		return false;
	}

	public boolean isEnabled() {
		return Main.instance != null;
	}

	public String getName() {
		return "UniEssentials Money";
	}

	public int fractionalDigits() {
		return -1;
	}

	public String currencyNamePlural() {
		return "";
	}

	public String currencyNameSingular() {
		return "";
	}
}