package net.mcuni.Uconomy;

import static net.mcuni.Util.CONSOLEDENY;
import static net.mcuni.Util.PER;
import static net.mcuni.Util.PLAYERISNOTFOUND;
import static net.mcuni.Util.isDoublePositive;
import static net.mcuni.Util.msg;
import static net.mcuni.Util.sendMessage;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.mcuni.Ussentials.Database.PlayerConfig;

public class EcoCommand implements CommandExecutor {

	EcoMain eco = new EcoMain();

	@SuppressWarnings("deprecation")
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (label.equalsIgnoreCase("money")) {
			if (!(sender instanceof Player)) {
				sendMessage(sender, CONSOLEDENY());
				return false;
			}

			Player target = (Player) sender;
			if (args.length == 0) {
				if (!eco.hasAccount(target.getName())) {
					sendMessage(sender, msg("NOACCOUNT").replace("%player%", target.getName()));
					return false;
				}
				sendMessage(sender, msg("PLAYERMONEY").replace("%player%", target.getName()).replace("%money%",
						String.valueOf(eco.getBalance(target.getName()))));
				return false;
			}
			if (args[0].equalsIgnoreCase("help")) {
				sendMessage(sender, msg("MONEYHELP"));
				return false;
			}
			if (args[0].equalsIgnoreCase("send")) {
				if (args.length != 3) {
					sendMessage(sender, msg("MONEYHELP"));
					return false;
				}
				target = Bukkit.getPlayer(args[1]);
				if (target == null) {
					sendMessage(sender, PLAYERISNOTFOUND());
					return false;
				}
				if (!isDoublePositive(args[2])) {
					sendMessage(sender, msg("ISNOTINTEGER"));
					return false;
				}
				if (!eco.has((Player) sender, Double.parseDouble(args[2]))) {
					sendMessage(sender, msg("ENOUGHMONEY"));
					return false;
				}
				eco.depositPlayer(target, Double.parseDouble(args[2]));
				eco.withdrawPlayer((Player) sender, Double.parseDouble(args[2]));
				sendMessage(sender, msg("SENDMONEY").replace("%player%", target.getName()).replace("%money%", args[2]));
				sendMessage(target, msg("GETMONEY").replace("%player%", sender.getName()).replace("%money%", args[2]));
				return false;
			}
			PlayerConfig cf = new PlayerConfig(Bukkit.getOfflinePlayer(args[0]).getUniqueId().toString());
			if (!cf.isFileExists()) {
				sendMessage(sender, msg("NOACCOUNT").replace("%player%", args[0]));
				return false;
			}
			sendMessage(sender, msg("PLAYERMONEY").replace("%player%", cf.getPlayerName()).replace("%money%",String.valueOf(eco.getBalance(cf.getPlayerName()))));
			return false;
		}

		if (label.equalsIgnoreCase("eco"))

		{
			if (!sender.hasPermission("Money.Admin")) {
				sendMessage(sender, PER("Money.Admin"));
				return false;
			}
			if (args.length != 3) {
				sendMessage(sender, msg("MONEYADMINHELP"));
				return false;
			}
			Player target = Bukkit.getPlayer(args[1]);
			if (target == null) {
				sendMessage(sender, PLAYERISNOTFOUND());
				return false;
			}
			if (!isDoublePositive(args[2])) {
				sendMessage(sender, msg("ISNOTINTEGER"));
				return false;
			}
			if (args[0].equalsIgnoreCase("set")) {
				eco.set(args[1], Double.parseDouble(args[2]));
				sendMessage(sender, msg("SETMONEY").replace("%player%", args[1]).replace("%money%", args[2]));
				sendMessage(target, msg("SETMONEYTARGET").replace("%money%", args[2]));
				return false;
			}
			if (args[0].equalsIgnoreCase("add")) {
				eco.depositPlayer(args[1], Double.parseDouble(args[2]));
				sendMessage(sender, msg("GIVEMONEY").replace("%player%", args[1]).replace("%money%", args[2]));
				sendMessage(target, msg("GIVEMONEYTARGET").replace("%money%", args[2]));
				return false;
			}
			if (args[0].equalsIgnoreCase("take")) {
				if (!eco.has(args[1], Double.parseDouble(args[2]))) {
					sendMessage(sender, msg("ENOUGHMONEYTARGET"));
					return false;
				}
				eco.withdrawPlayer(args[1], Double.parseDouble(args[2]));
				sendMessage(sender, msg("TAKEMONEY").replace("%player%", args[1]).replace("%money%", args[2]));
				sendMessage(target, msg("TAKEMONEYTARGET").replace("%money%", args[2]));
				return false;
			}
		}
		return false;
	}

}
